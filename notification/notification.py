import subprocess
from typing import Union

from constants import Line


def send(*args: Union) -> None:
    """Sends notification to command line and desktop.

     Args:
         args: Messages to sent.
     """

    message = ""

    for string in args:
        if string != Line.Types.BLANK:
            print("[ " + string + " ] ")

            message += "\n" + string
        else:
            message += "\n" + Line.Types.BLANK.value

    subprocess.call(["notify-send", "openpyn-simple-python", message])
