import getpass


def get_user() -> str:
    """Gets user desktop name.

     Returns:
        Desktop user.
     """

    return getpass.getuser()


PROJECT_PATH = "/home/" + get_user() + "/Documents/gitlab-projects/openpyn-simple-python/"
