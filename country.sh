#!/usr/bin/env bash

# get all available countries, exceptions are Egypt and United Arab Emirates
# curl --silent https://api.nordvpn.com/server | jq --raw-output '[.[].country] | sort | unique | .[]'

# to lowercase
readonly COUNTRY="${1,,}"

case ${COUNTRY} in

"al" | "albania")
  echo "Albania"
  ;;

"ar" | "argentina")
  echo "Argentina"
  ;;

"au" | "australia")
  echo "Australia"
  ;;

"at" | "austria")
  echo "Austria"
  ;;

"be" | "belgium")
  echo "Belgium"
  ;;

"ba" | "bosnia and herzegovina")
  echo "Bosnia and Herzegovina"
  ;;

"br" | "brazil")
  echo "Brazil"
  ;;

"bg" | "bulgaria")
  echo "Bulgaria"
  ;;

"ca" | "canada")
  echo "Canada"
  ;;

"cl" | "chile")
  echo "Chile"
  ;;

"cr" | "costa rica")
  echo "Costa Rica"
  ;;

"hr" | "croatia")
  echo "Croatia"
  ;;

"cy" | "cyprus")
  echo "Cyprus"
  ;;

"cz" | "czech republic")
  echo "Czech Republic"
  ;;

"dk" | "denmark")
  echo "Denmark"
  ;;

"ee" | "estonia")
  echo "Estonia"
  ;;

"fi" | "finland")
  echo "Finland"
  ;;

"fr" | "france")
  echo "France"
  ;;

"ge" | "georgia")
  echo "Georgia"
  ;;

"de" | "germany")
  echo "Germany"
  ;;

"gr" | "greece")
  echo "Greece"
  ;;

"hk" | "hong kong")
  echo "Hong Kong"
  ;;

"hu" | "hungary")
  echo "Hungary"
  ;;

"is" | "iceland")
  echo "Iceland"
  ;;

"in" | "india")
  echo "India"
  ;;

"id" | "indonesia")
  echo "Indonesia"
  ;;

"ie" | "ireland")
  echo "Ireland"
  ;;

"il" | "israel")
  echo "Israel"
  ;;

"it" | "italy")
  echo "Italy"
  ;;

"jp" | "japan")
  echo "Japan"
  ;;

"lv" | "latvia")
  echo "Latvia"
  ;;

"lu" | "luxembourg")
  echo "Luxembourg"
  ;;

"my" | "malaysia")
  echo "Malaysia"
  ;;

"mx" | "mexico")
  echo "Mexico"
  ;;

"md" | "moldova")
  echo "Moldova"
  ;;

"nl" | "netherlands")
  echo "Netherlands"
  ;;

"nz" | "new zealand")
  echo "New Zealand"
  ;;

"mk" | "north macedonia")
  echo "North Macedonia"
  ;;

"no" | "norway")
  echo "Norway"
  ;;

"pl" | "poland")
  echo "Poland"
  ;;

"pt" | "portugal")
  echo "Portugal"
  ;;

"ro" | "romania")
  echo "Romania"
  ;;

"rs" | "serbia")
  echo "Serbia"
  ;;

"sg" | "singapore")
  echo "Singapore"
  ;;

"sk" | "slovakia")
  echo "Slovakia"
  ;;

"si" | "slovenia")
  echo "Slovenia"
  ;;

"za" | "south africa")
  echo "South Africa"
  ;;

"kr" | "south korea")
  echo "South Korea"
  ;;

"es" | "spain")
  echo "Spain"
  ;;

"se" | "sweden")
  echo "Sweden"
  ;;

"ch" | "switzerland")
  echo "Switzerland"
  ;;

"tw" | "taiwan")
  echo "Taiwan"
  ;;

"th" | "thailand")
  echo "Thailand"
  ;;

"tr" | "turkey")
  echo "Turkey"
  ;;

"ua" | "ukraine")
  echo "Ukraine"
  ;;

"uk" | "united kingdom")
  echo "United Kingdom"
  ;;

"us" | "united states")
  echo "United States"
  ;;

"vn" | "vietnam")
  echo "Vietnam"
  ;;

*)
  echo "unknown"
  ;;
esac
