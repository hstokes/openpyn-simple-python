import subprocess

from constants import Line
from notification import notification
from openpyn_status import openpyn_status
from timing import timing


def get_ip() -> str:
    """Gets current IP address.

    Returns:
      IP address.
    """
    return subprocess.getoutput("curl --silent \"https://ifconfig.me/\"")


def disconnect() -> None:
    """Disconnects from VPN."""

    notification.send("Disconnecting.")
    subprocess.call(["openpyn", "-x"], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    if timing.wait_condition(lambda: openpyn_status.disconnected(), 8000):
        notification.send("Disconnected.", Line.Types.BLANK, "Default IP: " + get_ip())
    else:
        notification.send("Failed to disconnect.", Line.Types.BLANK,
                          "Check \"systemctl status openpyn\" for more details.")
