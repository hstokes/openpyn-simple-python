<div align="center">

![alt text](img/info.png "openpyn-simple")

# Simple usage of openpyn.

</div>

### Navigation

- [Features](#features)
- [Usage](#usage)
- [Examples](#examples)
- [Further Usage](#further-usage)

---
### Features

- killswitch and running as a daemon by default
- disconnect from one location and connect to another one (openpyn can't handle that itself)
- fancy output
- mac-changer-simple integration (change MAC address on every successful connection)&ast;

&ast; requires [mac-changer-simple](https://gitlab.com/hstokes/mac-changer-simple)

---
### Usage

`python3 app.py -c uk` 

`python3 app.py -c France`

`python3 app.py -c it -a Milano` 

`python3 app.py --disconnect`

Arguments *-f*, *-d*, *-t 3* and *-m 40* are used by default. Check out `python3 app.py --help` and  [openpyn usage options](https://github.com/jotyGill/openpyn-nordvpn#usage-options).

---
### Examples

Check out [examples folder](examples).

---
### Further Usage

Check out [bashrc-aliases](https://gitlab.com/hstokes/bashrc-aliases) project for even better usage.
