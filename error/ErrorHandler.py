import os
import time
from threading import Thread

from connection import connection
from constants import Line
from notification import notification
from openpyn_status import openpyn_status
from timing import timing

_end = False


def _should_end() -> bool:
    """Checks whether app should exit.

     Returns:
        True if app should end, false otherwise.
     """

    return _end


class ErrorHandler(Thread):
    """ErrorHandler Thread."""

    def __init__(self):
        """Initializes daemon thread."""

        Thread.__init__(self)
        self.daemon = True

    def run(self):
        """Executes daemon thread."""

        while _should_end() is False and openpyn_status.connected() is False:
            if openpyn_status.disconnected() is False:
                if timing.wait_condition(lambda: openpyn_status.received_error(), 3500):
                    global _end
                    _end = True
                    notification.send(openpyn_status.get_error_message(), Line.Types.BLANK,
                                      "Resetting back to default values.")
                    connection.disconnect()

                    # TODO concurrent thread shouldn't kill main one
                    os._exit(1)

            # 100 millis
            time.sleep(0.1)
