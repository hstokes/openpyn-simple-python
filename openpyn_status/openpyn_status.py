import re
import subprocess


def get_status() -> str:
    """Gets current daemon status.

     Returns:
        Status.
     """
    return subprocess.getoutput("systemctl status openpyn")


def status_contains(string: str) -> bool:
    """Checks whether status output contains string.

     Returns:
        True if status contains string, false otherwise.
     """

    return string in get_status()


def connected() -> bool:
    """Checks whether VPN connection is established.

     Returns:
        True if connected to VPN, false otherwise.
     """

    return status_contains("active (running)") and status_contains("Initialization Sequence Completed")


def get_country() -> str:
    """Gets country shortcut which is connection established to.

     Returns:
        Country which connection is established to.
     """

    _MATCH = re.findall("(?<=/usr/local/bin/openpyn\s)(\w+)", get_status())

    return _MATCH[0] if len(_MATCH) > 0 else ""


def get_area() -> str:
    """Gets current are within connected country.

     Returns:
        Area which connection is established to.
     """

    _MATCH = re.findall("(?<=--area ).*(?= --max-load)", get_status())

    return _MATCH[0] if len(_MATCH) > 0 else ""


def disconnected() -> bool:
    """Checks whether connection isn't established.

     Returns:
        True if disconnected, false otherwise.
     """

    return status_contains("Active: inactive (dead)") or status_contains("Active: failed")


def daemon_running() -> bool:
    """Checks whether daemon is running.

     Returns:
        True if daemon is running, false otherwise.
     """

    return status_contains("active (running)")


def get_error_message() -> str:
    """Gets error message from daemon output.

     Returns:
        Error message.
     """

    _MATCH = re.findall("\\[ERROR\\].*", get_status())

    return _MATCH[0] if len(_MATCH) > 0 else ""


def received_error() -> bool:
    """Checks whether error occurred.

     Returns:
        True if error occurred, false otherwise.
     """

    return len(get_error_message()) > 0


def auth_failed() -> bool:
    """Checks whether authentication failed.

     Returns:
        True if authentication failed, false otherwise.
     """

    return status_contains("AUTH_FAILED")
