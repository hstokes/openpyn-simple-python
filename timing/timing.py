import time
from typing import Callable


def wait_condition(condition: Callable, timeout: float) -> bool:
    """Wait condition for dynamic programming.

     Args:
         condition: Condition to run checks on.
         timeout: Should method exit if certain timeout passed.

     Returns:
        True if condition is met, false otherwise.
     """

    _START = time.time() * 1000 + timeout

    while condition() is False:
        if time.time() * 1000 >= _START:
            return False

        # 100 ms
        time.sleep(0.1)

    return True
