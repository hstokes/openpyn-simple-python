from enum import Enum


class Types(Enum):
    """Types class.

    Attributes:
        BLANK: Empty line.
    """

    BLANK = ""
