import argparse
import subprocess
import sys

import definitions
from connection import connection
from constants import Line
from error import ErrorHandler
from notification import notification
from openpyn_status import openpyn_status
from timing import timing


def main() -> None:
    _PARSER = argparse.ArgumentParser(description='openpyn-simple-python.')

    _PARSER.add_argument('-c', '--country', help='country name or shortcut (no difference in capitalization)')
    _PARSER.add_argument('-a', '--area', help='specific area to connect to, use quotes (eg. \"City of London\")')
    _PARSER.add_argument('-d', '--disconnect', action='store_true', help='kill connection and switch to default values')

    _ARGS = _PARSER.parse_args()

    # parse literal string from argument
    _COUNTRY = subprocess.getoutput(definitions.PROJECT_PATH + "country.sh " + repr(_ARGS.country))

    if _wrong_arguments(_ARGS):
        notification.send("Wrong arguments.")

        sys.exit(1)
    elif _should_disconnect(_ARGS):
        connection.disconnect()
        _execute_mac_changer_simple_restore_default()

        sys.exit(0)
    elif _wrong_country(_COUNTRY):
        notification.send("Wrong country.")

        sys.exit(1)

    _ERROR_HANDLER = ErrorHandler.ErrorHandler()
    _ARGS.area = "unknown" if _ARGS.area is None else _ARGS.area

    if openpyn_status.connected():
        if _should_move_to_another_location(_COUNTRY, _ARGS):
            notification.send("Disconnecting.")
            subprocess.call(["openpyn", "-x"], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

            if timing.wait_condition(lambda: openpyn_status.disconnected(), 8000) is False:
                notification.send("Failed to disconnect.")

                sys.exit(1)
        else:
            notification.send("Already connected to %s." % _ARGS.area, _COUNTRY,
                              "IP: " + connection.get_ip())

            sys.exit(0)

    _execute_mac_changer_simple()
    if _ARGS.area is not None and _ARGS.area != "unknown":
        subprocess.call(
            ["sudo", "openpyn", "-fd", _ARGS.country, "-a", repr(_ARGS.area), "-t", "3", "-m", "40"],
            stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    else:
        subprocess.call(["sudo", "openpyn", "-fd", _ARGS.country, "-t", "3", "-m", "40"],
                        stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    _ERROR_HANDLER.start()

    if timing.wait_condition(lambda: openpyn_status.daemon_running(), 10000) is False:
        notification.send("Failed to execute openpyn as daemon.")

        sys.exit(1)
    else:
        notification.send("Daemon started.", Line.Types.BLANK, "Establishing connection.")

    if timing.wait_condition(lambda: openpyn_status.connected(), 35000):
        notification.send("Connection established.", Line.Types.BLANK, "New IP: " + connection.get_ip(),
                          "Country: " + _COUNTRY, "Area: " + _ARGS.area)
    elif openpyn_status.auth_failed():
        for i in range(3):
            if timing.wait_condition(lambda: openpyn_status.connected(), 15000):
                notification.send("Connection established.", Line.Types.BLANK, "New IP: " + connection.get_ip(),
                                  "Country: " + _COUNTRY, "Area: " + _ARGS.area)

                sys.exit(0)

            notification.send("Authentication failed %d/3." % (i + 1))

        notification.send("Failed to authenticate.", Line.Types.BLANK, "Resetting back to default values.")
        connection.disconnect()
    else:
        notification.send("Failed to connect.", Line.Types.BLANK,
                          "Check \"systemctl status openpyn\" for more details.")

    sys.exit(1)


def _should_disconnect(args: argparse.Namespace) -> bool:
    return args.disconnect


def _wrong_arguments(args: argparse.Namespace) -> bool:
    # 1st argument is name of the app
    _PARSED_ARGUMENTS = len(sys.argv) - 1

    if args.disconnect is False and args.country is None or _PARSED_ARGUMENTS > 4:
        return True
    elif args.disconnect and args.country is not None or args.disconnect and args.area is not None:
        return True


def _wrong_country(country: str) -> bool:
    if country == "unknown":
        return True


def _should_move_to_another_location(expected_country: str, args: argparse.Namespace) -> bool:
    _ACTUAL_COUNTRY = subprocess.getoutput(
        definitions.PROJECT_PATH + "country.sh " + repr(openpyn_status.get_country()))
    _EXPECTED_AREA = args.area
    _ACTUAL_AREA = "unknown" if len(openpyn_status.get_area()) < 1 else openpyn_status.get_area()

    if _ACTUAL_COUNTRY != "unknown" and _ACTUAL_COUNTRY != expected_country:
        notification.send("Moving from %s to %s." % (_ACTUAL_COUNTRY, expected_country))

        return True
    elif _ACTUAL_AREA != _EXPECTED_AREA:
        notification.send("Moving from %s to %s." % (_ACTUAL_AREA, _EXPECTED_AREA))

        return True


def _execute_mac_changer_simple() -> None:
    subprocess.call(["/home/" + definitions.get_user() + "/Documents/gitlab-projects/mac-changer-simple/script.sh"])


def _execute_mac_changer_simple_restore_default() -> None:
    subprocess.call(
        ["/home/" + definitions.get_user() + "/Documents/gitlab-projects/mac-changer-simple/script.sh", "--default"])


if __name__ == '__main__':
    main()
